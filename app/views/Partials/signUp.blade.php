@extends('Layout.master')

@section('content')

<div class="container mt">
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1 centered">
        <h4>Your Personal Information</h4>
            <hr/>
        </div>

        <div class="col-lg-5 col-lg-offset-1">
            <div class="spacing"></div>
            {{ Form::open(array('route'=>'save.user','class'=>'form-horizontal','files'=>true,'enctype' => 'multipart/form-data','method'=>'POST')) }}

            <div class="form-group">
                    <label class="control-label col-sm-2" for="email">Name:</label>
                    <div class="col-sm-10">
                        <input type="text" name="name" class="form-control" id="email" placeholder="Enter Name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="email">Email:</label>
                    <div class="col-sm-10">
                        <input type="email" name="email" class="form-control" id="email" placeholder="Enter email" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="email">Phone:</label>
                    <div class="col-sm-10">
                        <input type="phone" name="phone" class="form-control" id="email" placeholder="Enter phone" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="email">Blood Group:</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="blood_group" id="" placeholder="Blood Group">
                            <option value="O+" selected>
                                O+
                            </option>
                            <option value="O-">
                                O-
                            </option>
                            <option value="A+">
                                A+
                            </option>
                            <option value="A-">
                                A-
                            </option>
                            <option value="B-">
                                B-
                            </option>
                            <option value="B+">
                                B+
                            </option>
                            <option value="AB+">
                                AB+
                            </option>
                            <option value="AB-">
                                AB-
                            </option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="email">Date Of Birth:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="birthDate" id="datepicker"> </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="email">Address:</label>
                <div class="col-sm-10">
                    <label class="control-label col-sm-2" for="email">Division</label>
                    <select class="form-control" name="division" id="division" required>

                        <option value="Dhaka">
                            Dhaka
                        </option>
                        <option value="Rajshahi">
                            Rajshahi
                        </option>
                        <option value="Sylhet">
                            Sylhet
                        </option>
                        <option value="Khulna">
                            Khulna
                        </option>
                        <option value="Chittagong">
                            Chittagong
                        </option>
                        <option value="Mymensing">
                            Mymensing
                        </option>

                    </select>

                    <label class="control-label col-sm-2" for="email">Address:</label>
                    <input type="text" name="house" class="form-control" id="house" placeholder="House" >
                    <label class="control-label col-sm-2" for="email">Post</label>
                    <input type="text" name="posts" class="form-control" id="post" placeholder="Post" required>
                    <label class="control-label col-sm-2" for="email">Village:</label>
                    <input type="text" name="village" class="form-control" id="village" placeholder="Village">
                    <label class="control-label col-sm-2" for="email">Street:</label>
                    <input type="text" name="street" class="form-control" id="village" placeholder="Street">
                </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-info">Next <i class="fa fa-angle-double-right"></i></button>
                    </div>
                </div>
            {{ Form::close() }}
        </div>

        <div class="col-lg-4 col-lg-offset-1">
            <!--div class="spacing"></div>
            <h4></h4>
            <img src="{{asset('img/career4.jpg')}}" class="img-thu" width="400px"/-->
            <div class="card">
                <img class="card-img-top" src="{{asset('img/career4.jpg')}}" data-src="{{asset('img/career4.jpg')}}" alt="Card image cap">
                <div class="card-block">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                    Find More.
                                </a>
                            </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse">
                            <div class="panel-body">
                                <h4>We help you to find the best possible higher education (Subjects,institutions).Keep Exploring..</h4><br/>
                                <a href="{{route('about')}}" class="btn btn-info">About Us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script>

        $(window).load(function() {

            $(document).ready(function () {
                $('html, body').scrollTop($(document).height() - $(window).height()-230);
            });

        });

    </script>


    <script>
    $(function() {
        $( "#datepicker" ).datepicker({
            changeYear:true,
            yearRange: "-200:+0",
        });
    });
    </script>
@stop
