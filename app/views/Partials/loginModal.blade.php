<div class="modal fade" id="loginModal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h6 class="">Already Registered? Sign In Now</h6>
            </div>
            <div class="modal-body">
                <div class="modal-body">

                    {{ Form::open(array('route'=>'user.login','class'=>'form-horizontal','files'=>true,'enctype' => 'multipart/form-data','method'=>'POST')) }}

                    <div class="form-group">
                        <label for="title">Email:</label>
                        <input type="email" name="email" class="form-control password" id="email" placeholder="Email" required>
                    </div>
                    <div class="form-group">
                        <label for="url">Password:</label>
                        <input type="password" name="password" class="form-control password" id="loginPop" placeholder="Phone Number" data-container="body" data-toggle="popover" data-placement="right" data-content="Your Phone Number Is The Password" required>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-group-lg"> Sign In </button>
                    </div>

                    {{ Form::close() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div>
</div>