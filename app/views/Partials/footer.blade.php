<div id="footerwrap">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <h4>About</h4>
                <div class="hline-w"></div>
                <p>What Next</p>
                <p>Email: erfceo@gmail.com</p>
            </div>
            <div class="col-lg-4">
                <h4>Social Links</h4>
                <div class="hline-w"></div>
                <p>
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-instagram"></i></a>
                </p>
            </div>
            <div class="col-lg-4">
                <h4>Address</h4>
                <div class="hline-w"></div>
                <p>
                    Flat : 2C<br/>
                    House : 48<br/>
                    Road : 6A <br/>
                    Dhanmondi Dhaka<br/>
                </p>
            </div>

        </div>
    </div>
</div>