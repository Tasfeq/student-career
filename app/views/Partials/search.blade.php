@extends('Layout.master')

@section('content')

<div class="container mtb">
    <div class="row">


        <div class="col-lg-8">

            <p><img class="" src="{{asset('img/career4.png')}}" width=500px"></p>

            <div class="spacing"></div>

        </div>


        <div class="col-lg-4">
            <a id="myResult" class="btn btn-success btn-lg">My result <i class="fa fa-institution"></i></a>
            <div class="spacing"></div>
            <h4>Welcome {{Auth::user()->name}}. Keep Exploring..</h4>
            <div class="hline"></div>
            <div class="spacing"></div>

            <div class="form-group">
                <label class="control-label " for="email">Major</label>
                <div class="col-sm-10">
                    <select class="form-control" name="background" id="background" placeholder="Background">
                        <option value="science" selected>
                            Science
                        </option>
                        <option value="commerce">
                            Commerce
                        </option>
                        <option value="arts">
                            Arts
                        </option>
                    </select>
                </div>
            </div>
            <div class="spacing"></div>

            <div class="form-group">
                <label class="control-label" for="email">SSC GPA</label>
                <div class="col-sm-10">
                    <input type="text" name="ssc" class="form-control" id="ssc" placeholder="SSC Gpa" required>
                </div>
            </div>
            <div class="spacing"></div>
            <div class="form-group">
                <label class="control-label" for="email">HSC GPA</label>
                <div class="col-sm-10">
                    <input type="text" name="hsc" class="form-control" id="hsc" placeholder="HSC GPA" required>
                </div>
            </div>
            <div class="spacing"></div>

            <div class="spacing"></div>
            <div class="form-group">
                <label class="control-label " for="email">Division</label>
                <div class="col-sm-10">
                    <select class="form-control" name="division" id="division" placeholder="">
                        <option value="Dhaka" selected>
                            Dhaka
                        </option>
                        <option value="Rajshahi">
                            Rajshahi
                        </option>
                        <option value="Khulna">
                            Khulna
                        </option>
                        <option value="Chittagong">
                            Chittagong
                        </option>
                        <option value="Sylhet">
                            Sylhet
                        </option>
                        <option value="Comilla">
                            Comilla
                        </option>
                        <option value="Mymensing">
                            Mymensing
                        </option>
                    </select>
                </div>
            </div>

            <div class="spacing"></div>

            <div class="form-group">
                <label class="control-label" for="email">Interest</label>
                <div class="col-sm-10">
                    <select class="form-control" name="interest" id="interest" required>

                        <option value="Engineering and Technology">
                            Engineering and Technology
                        </option>
                        <option value="Business Studies">
                            Business Studies
                        </option>
                        <option value="Law">
                            Law
                        </option>
                        <option value="Pharmacy">
                            Pharmacy
                        </option>
                        <option value="Science">
                            Science
                        </option>
                        <option value="Earth and Environmental Sciences">
                            Earth and Environmental Sciences
                        </option>
                        <option value="Engineering">
                            Engineering
                        </option>
                        <option value="Architecture and Planning">
                            Architecture and Planning
                        </option>
                        <option value="Architecture (Arch)">
                            Architecture (Arch)
                        </option>
                        <option value="Biological Science">
                            Biological Science
                        </option>
                        <option value="Agriculture">
                            Agriculture
                        </option>
                        <option value="Textile Engineering ">
                            Textile Engineering
                        </option>
                        <option value="Veterinary Science">
                            Veterinary Science
                        </option>

                    </select>
                </div>
            </div>
            <div class="spacing"></div>
            <div class="form-group">
                <label class="control-label" for="email"></label>
                <div class="col-sm-10 search">
                    <button type="submit" class="btn btn-info ">Go <i class="fa fa-search-plus fa-2x"></i></button>
                </div>
            </div>

            <div class="spacing"></div>

        </div>
    </div>
</div>


<div class="modal fade" id="resultModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                <p>One fine body&hellip;</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div class="modal fade" id="myResultModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                <p>One fine body&hellip;</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script>

    $(window).load(function() {

        $(document).ready(function () {
            $('html, body').scrollTop($(document).height() - $(window).height()-970);

        });

    });

</script>

<script>

    $(document).ready(function(){
        $(document).on('click','.search',function(e) {
            e.preventDefault();
            var background=$('#background').val();
            var ssc=$('#ssc').val();
            var hsc=$('#hsc').val();
            //var income=$('#income').val();
            var place=$('#division').val();
            var interest=$('#interest').val();

            var url = '{{ URL::route('search.result') }}';
            $.ajax({
                url: url,
                data: { 'background' :background , 'ssc' :ssc ,'hsc':hsc, 'place':place ,'interest':interest} ,
                type: "GET",
                success: function(response) {
                    //console.log(response);
                    setResult(response);

                },
                error: function(xhr, textStatus, thrownError) {
                    alert('Something went to wrong.Please Try again later...');
                }
            });

        });


        function setResult(response){

            $('#resultModal .modal-title').text("Available Universities and Respective Departments");
            var result = response.data;
            var formhtml = '';
            formhtml = '<table class="table">';
            formhtml += '<tr><th>University Name</th><th>Location</th><th>University Acronym</th><th>Website</th><th>Min GPA(SSC</th><th>Min GPA(HSC</th><th>Dept.Name</th></tr>';
//            for (var i = result.length - 1; i >= 0; i--) {
//                formhtml += '<tr><td>'+result[i].name+'</td><td>'+result[i].location+'</td><td>'+result[i].acronym+'</td><td><a target="_blank" href="'+result[i].website+'">'+result[i].website+'</a></td><td>'+result[i].department_name+'</td><td>'+result[i].ssc_gpa+'</td><td>'+result[i].hsc_gpa+'</td></tr>';
//            };
            $.each(result,function(key,value){
                console.log(value[0].name);
                formhtml += '<tr><td>'+key+'</td>' ;
                formhtml+='<td>'+value[0].location+'</td><td>'+value[0].acronym+'</td><td><a target="_blank" href="'+value[0].website+'">'+value[0].website+'</a></td><td>'+value[0].ssc_gpa+'</td><td>'+value[0].hsc_gpa+'</td>';
                deptName='<td><ul>';
                $.each(value,function(key,value){
                    deptName+='<li>'+value.department_name+'</li>';
                });
                deptName+='</ul></td><tr>';
                formhtml+=deptName;
            });
            formhtml += '</table>';
            $('#resultModal .modal-body').html(formhtml);
            $('#resultModal').modal('show');
        }
    })
</script>

<script>

    $('#background').on('change',function(){

        var value=$(this).val();
        //console.log(value);
        var data = {
            _token : '{{csrf_token()}}',
            input : value
        };

        $.ajax({

            type: "POST",
            url : "{{route('grab.result')}}",
            data : data,
            success : function(data){
                //console.log(data);
                clearData();
                dynamicOptions(data);
            }
        },"json");

        function clearData(){

            selectbox=document.getElementById('interest');
            var i;
            for(i = selectbox.options.length - 1 ; i >= 0 ; i--)
            {
                selectbox.remove(i);
            }
        }

        function dynamicOptions(data){

            var values=data.result;
            select = document.getElementById('interest');

            //console.log(values[0]);

            for(var i = 0; i<=values.length-1; i++){
                var opt = document.createElement('option');
                opt.value = values[i];
                opt.text = values[i];
                select.add( opt );
            }

        }
    })

</script>

    <script>

        $(document).ready(function(){

            $(document).on('click','#myResult',function(e) {
                e.preventDefault();

                var url = '{{ URL::route('search.myResult') }}';
                $.ajax({
                    url: url,
                    type: "GET",
                    success: function(response) {

                        console.log(response);
                        setMyResult(response);

                    },
                    error: function(xhr, textStatus, thrownError) {
                        alert('Something went to wrong.Please Try again later...');
                    }
                });

            });

            function setMyResult(response){

                $('#myResultModal .modal-title').text("Available Universities and Respective Departments");
                var result = response.data;
                console.log(result)
                formhtml = '<table class="table">';
                formhtml += '<tr><th>University Name</th><th>Location</th><th>University Acronym</th><th>Website</th><th>Min GPA(SSC</th><th>Min GPA(HSC</th><th>Dept.Name</th></tr>';
//            for (var i = result.length - 1; i >= 0; i--) {
//                formhtml += '<tr><td>'+result[i].name+'</td><td>'+result[i].location+'</td><td>'+result[i].acronym+'</td><td><a target="_blank" href="'+result[i].website+'">'+result[i].website+'</a></td><td>'+result[i].department_name+'</td><td>'+result[i].ssc_gpa+'</td><td>'+result[i].hsc_gpa+'</td></tr>';
//            };
                $.each(result,function(key,value){
                    console.log(value[0].name);
                    formhtml += '<tr><td>'+key+'</td>' ;
                    formhtml+='<td>'+value[0].location+'</td><td>'+value[0].acronym+'</td><td><a target="_blank" href="'+value[0].website+'">'+value[0].website+'</a></td><td>'+value[0].ssc_gpa+'</td><td>'+value[0].hsc_gpa+'</td>';
                    deptName='<td><ul>';
                    $.each(value,function(key,value){
                        deptName+='<li>'+value.department_name+'</li>';
                    });
                    deptName+='</ul></td><tr>';
                    formhtml+=deptName;
                });
                formhtml += '</table>';
                $('#myResultModal .modal-body').html(formhtml);
                $('#myResultModal').modal('show');
            }
        })

    </script>

@stop