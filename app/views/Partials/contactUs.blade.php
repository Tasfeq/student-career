@extends('Layout.master')

@section('content')


    <div class="container mtb">
    <div class="row">
        <div class="col-lg-8">
            <h4>Give Us Your Valuable Feedback!</h4>
            <div class="hline"></div>
                <p>Fill The Form Below</p>
                @if(Session::has('message'))
                <div class="alert alert-success">
                    <h4>{{ Session::get('message') }}</h4>
                </div>
                @endif

            {{ Form::open(array('route'=>'save.contact.form','class'=>'cd-form floating-labels','files'=>true,'enctype' => 'multipart/form-data','method'=>'POST')) }}

                <div class="form-group">
                    <label for="InputName1">Your Name</label>
                    <input type="text" name="name" class="form-control" id="exampleInputEmail1" required>
                </div>
                <div class="form-group">
                    <label for="InputEmail1">Email address</label>
                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" required>
                </div>
                <div class="form-group">
                    <label for="InputSubject1">Subject</label>
                    <input type="text" name="subject" class="form-control" id="exampleInputEmail1">
                </div>
                <div class="form-group">
                    <label for="message1">Message</label>
                    <textarea class="form-control" name="message" id="message1" rows="6" required></textarea>
                </div>
                <button type="submit" class="btn btn-success">Submit</button>

            {{ Form::close() }}
        </div>

        <div class="col-lg-4">
            <h4>Our Address</h4>
            <div class="hline"></div>
            <p>
                Flat : 2C<br/>
                House : 48<br/>
                Road : 6A <br/>
                Dhanmondi Dhaka<br/>
            </p>
            <p>Email: erfceo@gmail.com</p>

            <p>Thank You.</p>
        </div>

        <div class="col-lg-4">
            <div id="googleMap" style="width:390px;height:300px;" ></div>

        </div>
    </div>
</div>

    <script>

        $(window).load(function() {

            $(document).ready(function () {
                $('html, body').scrollTop($(document).height() - $(window).height()-320);

            });

        });

    </script>


    <script>
        var map;
        var myCenter=new google.maps.LatLng(23.744671,90.372896);

        function initialize()
        {

            var mapProp = {
                center:myCenter,
                zoom:14,
                mapTypeId:google.maps.MapTypeId.ROADMAP
            };

            map = new google.maps.Map(document.getElementById("googleMap"),mapProp);

            google.maps.event.addListener(map, 'click', function(event) {
                placeMarker(event.latLng);
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>

@stop