
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{route('home')}}">What Next</a>
        </div>
        <div class="navbar-collapse collapse navbar-right">
            <ul class="nav navbar-nav">
                <li class="active"><a href="{{route('home')}}">HOME</a></li>
                <li class="about-tab"><a href="{{route('about')}}">ABOUT US</a></li>
                <li class="contact-tab"><a href="{{route('contact')}}">CONTACT</a></li>
                @if(Auth::user())
                <li><a href="{{route('logout')}}">Sign Out</a></li>
                @else
                    <li><a href="" class="loginModal">Login</a></li>
                @endif
                @if(Auth::user())
                    <li class="search-tab"><a href="{{route('search')}}">Search</a></li>
                @endif
            </ul>
        </div>
    </div>
</div>


