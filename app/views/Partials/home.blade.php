
<div id="headerwrap">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h3>Welcome To Next Step</h3>
                <h1>Your Higher Education and Career Solution</h1>
                <h1><a href="{{route('signup')}}" class="btn btn-success btn-lg">Get Started</a></h1>

            </div>

            <div class="col-lg-8 col-lg-offset-2 himg">
                <img src="{{asset('img/career1.jpg')}}" class="img-responsive">

            </div>

        </div><!-- /row -->
    </div> <!-- /container -->
</div><!-- /headerwrap -->