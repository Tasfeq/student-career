@extends('Layout.master')

@section('content')

    <div class="container mtb">

        <div class="row">
            <div class="col-lg-6">
                <img class="img-responsive" src="{{asset('img/career2.jpg')}}" alt="" width="400px" height="800px">
            </div>

            <div class="col-lg-6">
                <h4>What Next</h4>
                <p>An initiative of education research foundation</p>
                <p><br/><a href="{{route('contact')}}" class="btn btn-primary">Contact Us</a></p>
            </div>
        </div>
    </div>


    <script>

        $(window).load(function() {

            $(document).ready(function () {
                $('html, body').scrollTop($(document).height() - $(window).height()-230);

            });

        });

    </script>
@stop
