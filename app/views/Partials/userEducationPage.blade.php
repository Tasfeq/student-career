@extends('Layout.master')

@section('content')

    <div class="container mt">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 centered">
                <h4>Your Educational Information</h4>
                <hr/>
            </div>

            <div class="col-lg-5 col-lg-offset-1">
                <div class="spacing"></div>
                {{ Form::open(array('route'=>'save.user.education','class'=>'form-horizontal','files'=>true,'enctype' => 'multipart/form-data','method'=>'POST')) }}

                <div class="form-group">
                    <label class="control-label col-sm-2" for="email">Background</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="background" id="background" placeholder="">
                            <option value="science" selected>
                                Science
                            </option>
                            <option value="commerce">
                                Commerce
                            </option>
                            <option value="arts">
                                Arts
                            </option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="email">SSC Grade :</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="ssc_grade" id="" placeholder="ssc grade">
                            <option value="A+" selected>
                                A+
                            </option>
                            <option value="A-">
                                A-
                            </option>
                            <option value="A">
                                A
                            </option>
                            <option value="B+">
                                B+
                            </option>
                            <option value="B-">
                                B-
                            </option>
                            <option value="B">
                                B
                            </option>
                            <option value="C+">
                                C+
                            </option>
                            <option value="C-">
                                C-
                            </option>
                            <option value="C">
                                C
                            </option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="email">SSC GPA :</label>
                    <div class="col-sm-10">
                        <input type="phone" name="ssc" class="form-control" id="email" placeholder="SSC GPA" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="email">HSC Grade :</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="hsc_grade" id="" placeholder="hsc grade">
                            <option value="A+" selected>
                                A+
                            </option>
                            <option value="A-">
                                A-
                            </option>
                            <option value="A">
                                A
                            </option>
                            <option value="B+">
                                B+
                            </option>
                            <option value="B-">
                                B-
                            </option>
                            <option value="B">
                                B
                            </option>
                            <option value="C+">
                                C+
                            </option>
                            <option value="C-">
                                C-
                            </option>
                            <option value="C">
                                C
                            </option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="email">HSC GPA :</label>
                    <div class="col-sm-10">
                        <input type="phone" name="hsc" class="form-control" id="email" placeholder="HSC GPA" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="email">Interest</label>
                    <div class="col-sm-10">
                            <select class="form-control" name="interest" id="interest" required>

                                <option value="Engineering and Technology">
                                    Engineering and Technology
                                </option>
                                <option value="Business Studies">
                                    Business Studies
                                </option>
                                <option value="Law">
                                    Law
                                </option>
                                <option value="Pharmacy">
                                    Pharmacy
                                </option>
                                <option value="Science">
                                    Science
                                </option>
                                <option value="Earth and Environmental Sciences">
                                    Earth and Environmental Sciences
                                </option>
                                <option value="Engineering">
                                    Engineering
                                </option>
                                <option value="Architecture and Planning">
                                    Architecture and Planning
                                </option>
                                <option value="Architecture (Arch)">
                                    Architecture (Arch)
                                </option>
                                <option value="Biological Science">
                                    Biological Science
                                </option>
                                <option value="Agriculture">
                                    Agriculture
                                </option>
                                <option value="Textile Engineering ">
                                    Textile Engineering
                                </option>
                                <option value="Veterinary Science">
                                    Veterinary Science
                                </option>

                            </select>
                        </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="email">Family Income</label>
                    <div class="col-sm-10">
                        <input type="text" name="income" class="form-control" id="email" placeholder="Enter Annual Income" >
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Save <i class="fa fa-angle-double-right"></i></button>
                    </div>
                </div>

                <input type="hidden" name="id" value="{{$userId}}"/>
                {{ Form::close() }}
            </div>

            <div class="col-lg-4 col-lg-offset-1">
                <div class="spacing"></div>
                <h4></h4>
                <img src="{{asset('img/edu.jpg')}}" width="400px"/>
            </div>

        </div>
    </div>

    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script>

        $(window).load(function() {

            $(document).ready(function () {
                $('html, body').scrollTop($(document).height() - $(window).height()-230);

            });

        });

    </script>


    <script>
        $(function() {
            $( "#datepicker" ).datepicker();
        });
    </script>

    <script>

        $('#background').on('change',function(){

            var value=$(this).val();
            //console.log(value);
            var data = {
                _token : '{{csrf_token()}}',
                input : value
            };

            $.ajax({

                type: "POST",
                url : "{{route('grab.result')}}",
                data : data,
                success : function(data){
                    //console.log(data);
                    clearData();
                    dynamicOptions(data);
                }
            },"json");

            function clearData(){

                selectbox=document.getElementById('interest');
                var i;
                for(i = selectbox.options.length - 1 ; i >= 0 ; i--)
                {
                    selectbox.remove(i);
                }
            }

            function dynamicOptions(data){

                var values=data.result;
                select = document.getElementById('interest');

                //console.log(values[0]);

                for(var i = 0; i<=values.length-1; i++){
                    var opt = document.createElement('option');
                    opt.value = values[i];
                    opt.text = values[i];
                    select.add( opt );
                }

            }
        })

    </script>
@stop
