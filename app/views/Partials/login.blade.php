@extends('Layout.master')

@section('content')



    <div class="container mt">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 centered">
                <h4>Login</h4>
                <hr/>
            </div>

            <div class="col-lg-5 col-lg-offset-1">
                <div class="spacing"></div>
                {{ Form::open(array('route'=>'user.login','class'=>'form-horizontal','files'=>true,'enctype' => 'multipart/form-data','method'=>'POST')) }}

                <div class="input-group">
                    <span class="input-group-addon" id="sizing-addon2">Email</span>
                    <input type="text" name="email" class="form-control" placeholder="" aria-describedby="sizing-addon2" required>
                </div>
                <hr/>
                <div class="input-group">
                    <span class="input-group-addon" id="sizing-addon2">Password</span>
                    <input type="password" id="example" name="password" class="form-control" placeholder="Phone Number" aria-describedby="sizing-addon2" data-container="body" data-toggle="popover" data-placement="right" data-content="Your Phone Number Is The Password" required>
                </div>

                <hr/>
                <div class="input-group">
                    <div class="">
                        <button type="submit" class="btn btn-default right">SignIn <i class="fa fa-key"></i></button>
                    </div>
                </div>

                {{ Form::close() }}
                <div class="form-group">
                    <label class="control-label col-sm-2" for="email"></label>
                    <div class="col-sm-10">

                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-lg-offset-1">
                <div class="spacing"></div>
                <h4></h4>
                <img src="{{asset('img/login.png')}}" width="350px"/>
            </div>

            <hr/>
        </div>
    </div>






    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>

        $(window).load(function() {

            $(document).ready(function () {
                $('html, body').scrollTop($(document).height() - $(window).height()-230);
                $('#example').popover();

            });

        });

    </script>
@stop
