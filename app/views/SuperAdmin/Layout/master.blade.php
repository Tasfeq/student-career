<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Admin Panel For University Solution</title>

    {{ HTML::style('adminPanel/css/style-responsive.css') }}
    {{ HTML::style('adminPanel/css/style.css') }}
    {{ HTML::style('adminPanel/css/zabuto_calendar.css') }}
    {{ HTML::style('adminPanel/css/jquery.gritter.css') }}
    {{ HTML::style('adminPanel/lineicons/style.css') }}
    {{ HTML::style('adminPanel/font-awesome/css/font-awesome.css') }}
    {{ HTML::style('adminPanel/css/bootstrap.css') }}

    {{ HTML::script('adminPanel/js/chart-master/Chart.js') }}
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<section id="container" >

    <header class="header black-bg">
        <div class="sidebar-toggle-box">
            <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
        </div>

        <div class="top-menu">
            <ul class="nav pull-right top-menu">
                <li><a class="logout" href="{{route('logout')}}">Logout</a></li>
            </ul>
        </div>
    </header>
    <!--header end-->

    <!--sidebar start-->

            @include('SuperAdmin.Partials.sidebar')

    <!--sidebar end-->

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">

            @yield('content')

        </section>
    </section>

    <!--main content end-->

    <!--footer start-->

    <!--footer end-->
</section>


{{ HTML::script('adminPanel/js/jquery.js') }}
{{ HTML::script('adminPanel/js/jquery-1.8.3.min.js') }}
{{ HTML::script('adminPanel/js/bootstrap.min.js') }}
{{ HTML::script('adminPanel/js/jquery.dcjqaccordion.2.7.js') }}
{{ HTML::script('adminPanel/js/jquery.scrollTo.min.js') }}
{{ HTML::script('adminPanel/js/jquery.nicescroll.js') }}
{{ HTML::script('adminPanel/js/jquery.sparkline.js') }}
{{ HTML::script('adminPanel/js/common-scripts.js') }}
{{ HTML::script('adminPanel/js/gritter/js/jquery.gritter.js') }}
{{ HTML::script('adminPanel/js/gritter-conf.js') }}
{{ HTML::script('adminPanel/js/sparkline-chart.js') }}
{{ HTML::script('adminPanel/js/zabuto_calendar.js') }}


<script type="text/javascript">
    $(document).ready(function () {
        var unique_id = $.gritter.add({
            // (string | mandatory) the heading of the notification
            title: 'Welcome to Dashgum!',
            // (string | mandatory) the text inside the notification
            text: '',
            // (string | optional) the image to display on the left
            image: 'assets/img/ui-sam.jpg',
            // (bool | optional) if you want it to fade out on its own or just sit there
            sticky: true,
            // (int | optional) the time you want it to be alive for before fading out
            time: '',
            // (string | optional) the class name you want to apply to that specific message
            class_name: 'my-sticky-class'
        });

        return false;
    });
</script>

<script type="application/javascript">
    $(document).ready(function () {
        $("#date-popover").popover({html: true, trigger: "manual"});
        $("#date-popover").hide();
        $("#date-popover").click(function (e) {
            $(this).hide();
        });

        $("#my-calendar").zabuto_calendar({
            action: function () {
                return myDateFunction(this.id, false);
            },
            action_nav: function () {
                return myNavFunction(this.id);
            },
            ajax: {
                url: "show_data.php?action=1",
                modal: true
            },
            legend: [
                {type: "text", label: "Special event", badge: "00"},
                {type: "block", label: "Regular event", }
            ]
        });
    });


    function myNavFunction(id) {
        $("#date-popover").hide();
        var nav = $("#" + id).data("navigation");
        var to = $("#" + id).data("to");
        console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
    }
</script>

@yield('page-specific-js')

</body>
</html>
