@extends('SuperAdmin.Layout.master')

@section('content')

<div class="row mt">
    <div class="col-md-12">
        <div class="content-panel">
            <div class="row">
                <div class="col-sm-12">
                    <h3>Users</h3>
                </div>
                <div class="col-sm-12">
                    <table id="usersTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone No.</th>
                            <th>Address</th>
                            <th>SSC GPA</th>
                            <th>HSC GPA</th>
                            <th>Annual Income</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($users as $key=>$user)

                        <tr>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->phone}}</td>
                            <td>{{$user->address}}</td>
                            <td>{{$user->ssc}}</td>
                            <td>{{$user->hsc}}</td>
                            <td>{{$user->annual_income}}</td>
                        </tr>

                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('page-specific-js')

    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $('#usersTable').dataTable({
            "order": [],
            "oLanguage": {
                "sInfo": "Showing _START_ to _END_ of _TOTAL_ Users",
                "sInfoEmpty": "Showing 0 to 0 of 0 Users",
                "sEmptyTable": "No information available"
            }
        });
    })
</script>

@stop