<aside>
    <div id="sidebar"  class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">

            <p class="centered"><img src="{{asset('img/user.png')}}" class="img-circle" width="60"></p>
            <h5 class="centered">{{Auth::user()->name}}</h5>

            <li class="mt">
                <a class="active" href="{{route('admin.home')}}">
                    <i class="fa fa-dashboard"></i>
                    <span>Home</span>
                </a>
            </li>

            <li class="mt">
                <a class="" href="{{route('admin.dashboard')}}">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            <li class="sub-menu">
                <a href="javascript:;" >
                    <i class="fa fa-cogs"></i>
                    <span>Features</span>
                </a>
                <ul class="sub">
                    <li><a  href="{{route('admin.users')}}">Users</a></li>
                    <li><a  href="{{route('admin.calendar')}}">Calendar</a></li>
                </ul>
            </li>

        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>