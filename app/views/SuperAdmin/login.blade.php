<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Login</title>

    {{ HTML::style('adminPanel/css/bootstrap.css') }}
    {{ HTML::style('adminPanel/font-awesome/css/font-awesome.css') }}
    {{ HTML::style('adminPanel/css/style.css') }}
    {{ HTML::style('adminPanel/css/style.css') }}
    {{ HTML::style('adminPanel/css/style-responsive.css') }}


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<!-- **********************************************************************************************************************************************************
MAIN CONTENT
*********************************************************************************************************************************************************** -->

<div id="login-page">
    <div class="container">

        {{ Form::open(array('route'=>'admin.login','class'=>'form-login','files'=>true,'enctype' => 'multipart/form-data','method'=>'POST')) }}
            <h2 class="form-login-heading">University Solution Admin Panel
                @if(Session::has('errorLogin'))
                    <div class="alert alert-danger">
                        <h5>{{ Session::get('errorLogin') }}</h5>
                    </div>
                @endif
            </h2>
            <div class="login-wrap">
                <input type="text" name="email" class="form-control" placeholder="User Email" autofocus required>
                <br>
                <input type="password" name="password" class="form-control" placeholder="Password" required>

                <label class="checkbox">
		                <span class="pull-right">
                        </span>
                </label>

                <button class="btn btn-theme btn-block" type="submit"><i class="fa fa-lock"></i> SIGN IN</button>
                <hr>
            </div>
        {{ Form::close() }}

    </div>
</div>

{{ HTML::script('adminPanel/js/bootstrap.min.js') }}
{{ HTML::script('adminPanel/js/jquery.js') }}
{{ HTML::script('adminPanel/js/jquery.backstretch.min.js') }}

<script>
    $.backstretch("assets/img/login-bg.jpg", {speed: 500});
</script>

</body>
</html>
