<?php

use Illuminate\Support\Facades\Redirect;

class AdminController extends \BaseController {

    public function signUp(){

		return View::make('Partials.signUp');

	}

	public function saveUser(){
		$user=new \User();
		$user->name=Input::get('name');
		$user->email=Input::get('email');
		$user->password=\Illuminate\Support\Facades\Hash::make(Input::get('phone'));
		$user->phone=Input::get('phone');
		$user->address=Input::get('division');
		$user->house=Input::get('house');
		$user->post=Input::get('posts');
		$user->village=Input::get('village');
		$user->street=Input::get('street');
		$user->birth_date=Input::get('birthDate');
		$user->blood_type=Input::get('blood_group');

		$user->save();
		$id=$user->id;
		//return redirect::back()->with('id');
		return Redirect::route('user.education',$id);
	}

	public function getUserEducationData($id){

		$userId=$id;
		return View::make('Partials.userEducationPage',compact('userId'));

	}

	public function saveUserEducationData(){

       $user=\User::find(Input::get('id'));
		$user->background=Input::get('background');
		$user->ssc=Input::get('ssc');
		$user->hsc=Input::get('hsc');
	   $user->hsc_grade=Input::get('hsc_grade');
	   $user->ssc_grade=Input::get('ssc_grade');
	   $user->annual_income=Input::get('income');
	   $user->interest=Input::get('interest');
	   $user->status=2;
	   $user->save();

		return Redirect::route('login');

	}

    public function login(){

		return View::make('Partials.login');
	}

	public function emailLogin(){
		$credentials = array(
			'email' => Input::get('email'),
			'password' => Input::get('password')
		);
		if(Auth::attempt($credentials)){

			return View::make('Partials.search');
		}else{
			return redirect::back();
		}
	}

	public function logout(){

		Auth::logout();
		Session::flush();
		return Redirect::route('home');
	}

	public function search(){

		return View::make('Partials.search');
	}

	public function superAdminLogin(){

		return View::make('SuperAdmin.login');
	}

	public function superAdminLoginCheck(){

		$credentials = array(
			'email' => Input::get('email'),
			'password' => Input::get('password')
		);
		if(Auth::attempt($credentials)){

            $status=Auth::user()->status;
			if($status==1){
				return View::make('SuperAdmin.Partials.home');
			}else{
				Session::flash('errorLogin',"You Are Not Allowed Here");
				return redirect::back();
			}
		}else{
			Session::flash('errorLogin',"Invalid Credentials");
			return redirect::back();
		}
	}

	public function showAllUsers(){

		$users=\User::where('status','!=',1)->get();
		return View::make('SuperAdmin.Partials.users',compact('users'));
	}

	public function showDashboard(){

		if(Auth::user()){
		return View::make('SuperAdmin.Partials.dashboard');
		}else{
			return redirect::back();
		}
	}

	public function showCalendar(){

		return View::make('SuperAdmin.Partials.calendar');
	}

	public function showHome(){

		return View::make('SuperAdmin.Partials.home');
	}
}
