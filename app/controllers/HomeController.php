<?php

use Illuminate\Support\Facades\Redirect;

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index()
	{
		return View::make('Layout.master');
	}

	public function getAboutUs(){

		return View::make('Partials.aboutUs');
	}

	public function getContactUs(){
		
		return View::make('Partials.contactUs');
	}

	public function saveContact(){

		$contact= new \Contact();
		$contact->name=Input::get('name');
		$contact->email=Input::get('email');
		$contact->subject=Input::get('subject');
		$contact->message=Input::get('message');
		$contact->status="Published";
		$contact->save();

		Session::flash('message',"Message Successfully Sent");
		Return redirect::back();
	}

   public function searchResult(){

	   //$income=Input::get('income');
	   $background=Input::get('background');
	   $ssc=Input::get('ssc');
	   $hsc=Input::get('hsc');
	   //$division=Input::get('place');
	   $interest=Input::get('interest');
	   //$gpa=$ssc+$hsc;

       $query= DB::table('universities')
		   ->rightJoin('degrees', 'universities.id', '=', 'degrees.university_id')
		   ->rightJoin('requirements', 'universities.id', '=', 'requirements.university_id')
		   ->rightJoin('departments', 'universities.id', '=', 'departments.university_id')
		   //->where('universities.location','=',$division)
		   ->where('requirements.background','=',$background)
		   ->where('departments.faculty_name','=',$interest)
		   ->where('requirements.ssc_gpa','<=',$ssc)
		   ->where('requirements.hsc_gpa','<=',$hsc)
		   ->groupBy('departments.id')
		   ->select('universities.name','degrees.degree','requirements.background','universities.location','universities.website',
			       'departments.department_name','universities.acronym','requirements.ssc_gpa','requirements.hsc_gpa')

		   ->get();
	   return Response::json(['success'=>true,
		   'data'=>$this->customizedData($query)
	   ]);
   }

	public function grabData(){

		$value=Input::get('input');

		if($value=="science"){

			$data=array("Engineering and Technology","Business Studies","Law","Pharmacy","Science","Earth and Environmental Sciences",
				"Engineering","Architecture and Planning","Architecture (Arch)","Biological Science","Agriculture",
				"Textile Engineering","Veterinary Science");

		}elseif($value=="commerce"){

			$data=array("Business Studies","Social Sciences","Islam");

		}else{

			$data=array("Arts","Fine Arts");
		}

		return Response::json(['success'=>true,
			'result'=>$data
		]);
	}

	public function searchMyResult(){

		//$id=Auth::user()->id;

		$background=Auth::user()->background;
		$ssc=Auth::user()->ssc;
		$hsc=Auth::user()->hsc;
		$interest=Auth::user()->interest;

		//$division=Auth::user()->address;

		$query= DB::table('universities')
			->rightJoin('degrees', 'universities.id', '=', 'degrees.university_id')
			->rightJoin('requirements', 'universities.id', '=', 'requirements.university_id')
			->rightJoin('departments', 'universities.id', '=', 'departments.university_id')
			//->where('universities.location','=',$division)
			->where('requirements.background','=',$background)
			->where('departments.faculty_name','=',$interest)
			->where('requirements.ssc_gpa','<=',$ssc)
			->where('requirements.hsc_gpa','<=',$hsc)
			->groupBy('departments.id')
			->select('universities.name','degrees.degree','requirements.background','universities.location','universities.website',
				'departments.department_name','universities.acronym','requirements.ssc_gpa','requirements.hsc_gpa')

			->get();

		return Response::json(['success'=>true,
			'data'=>$this->customizedData($query)
		]);
	}

	private function customizedData($data){
		$customizedData = [];
		$universityName = [];
		foreach($data as $item){
			if(!in_array($item->name,$universityName)){
				array_push($universityName,$item->name);
			}
			$index = !empty($customizedData[$item->name])?count($customizedData[$item->name]):0;
			$customizedData[$item->name][$index] = $item;
		}
		return $customizedData;
	}
}
