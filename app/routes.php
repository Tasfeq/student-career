<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// ***User Routes*** //

Route::get('/sign-up', array('as' => 'signup', 'uses' => 'AdminController@signUp'));
Route::post('/save-user', array('as' => 'save.user', 'uses' => 'AdminController@saveUser'));
Route::get('/user-education/{id}', array('as' => 'user.education', 'uses' => 'AdminController@getUserEducationData'));
Route::post('/save-user-education', array('as' => 'save.user.education', 'uses' => 'AdminController@saveUserEducationData'));
Route::post('/grab-result', array('as' => 'grab.result', 'uses' => 'HomeController@grabData'));
Route::get('/login', array('as' => 'login', 'uses' => 'AdminController@login'));
Route::post('/post-login', array('as' => 'user.login', 'uses' => 'AdminController@emailLogin'));
Route::get('logout',array('as'=>'logout','uses'=>'AdminController@logout'));

// ***Static Page Routes*** //

Route::get('/', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::get('/about', array('as' => 'about', 'uses' => 'HomeController@getAboutUs'));
Route::get('/contact', array('as' => 'contact', 'uses' => 'HomeController@getContactUs'));
Route::post('/save-contact', array('as' => 'save.contact.form', 'uses' => 'HomeController@saveContact'));
Route::get('/search', array('as' => 'search', 'uses' => 'AdminController@search'));

// ***Super Admin Routes*** //

Route::get('/admin',array('as'=>'superadmin','uses'=>'AdminController@superAdminLogin'));
Route::post('/admin-login',array('as'=>'admin.login','uses'=>'AdminController@superAdminLoginCheck'));
Route::get('/admin-home',array('as'=>'admin.home','uses'=>'AdminController@showHome'));
Route::get('/admin-users',array('as'=>'admin.users','uses'=>'AdminController@showAllUsers'));
Route::get('/admin-dashboard',array('as'=>'admin.dashboard','uses'=>'AdminController@showDashboard'));
Route::get('/admin-calendar',array('as'=>'admin.calendar','uses'=>'AdminController@showCalendar'));


// ***User Routes*** //

Route::get('/search-result',array('as'=>'search.result','uses'=>'HomeController@searchResult'));
Route::get('/search-myResult',array('as'=>'search.myResult','uses'=>'HomeController@searchMyResult'));

Route::get('/test',array('as'=>'search.result','uses'=>'HomeController@searchResult'));