<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		 $this->call('UserTableSeeder');
	}

}

class UserTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('users')->delete();
		User::create(array(

			'name' => 'admin',
		    'email' => 'admin@yahoo.com',
		    'password' => \Illuminate\Support\Facades\Hash::make('01611111111'),
		    'phone' => '01611111111',
		    'address' =>'Dhanmondi',
		    'post' =>'Not Applicable',
		    'blood_type' =>'Not Applicable+',
		    'birth_date' =>'Not Applicable',
		));
	}

}
